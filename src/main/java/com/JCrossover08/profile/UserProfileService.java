package com.JCrossover08.profile;

import com.JCrossover08.bucket.BucketName;
import com.JCrossover08.filestore.FileStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

import static org.apache.http.entity.ContentType.*;

@Service
public class UserProfileService {

    private final UserProfileDataAccessService userProfileDataAccessService;
    private final FileStore fileStore;

    @Autowired
    public UserProfileService(UserProfileDataAccessService userProfileDataAccessService, FileStore fileStore) {
        this.userProfileDataAccessService = userProfileDataAccessService;
        this.fileStore = fileStore;
    }

    public List<UserProfile> getUserProfiles(){
        return userProfileDataAccessService.getUserProfiles();
    }

    public void uploadUserProfileİmage(UUID userProfileId, MultipartFile file){

        // 1. Check imagge  isnt empty
        isFileEmpty(file);


        // 2. If file is an immage
        isİmage(file);


        //  3. The user exit in our database
        UserProfile user = getUserProfileorThrow(userProfileId);

        // 4. Grap some metadata from file if any
        Map<String, String> metadata = extractMetadata(file);


        //  5. Store the image in s3 and update database (userProfileImageLink) with s33  image link
        String path = String.format("%s/%s", BucketName.PROFILE_IMAGE.getBucketName(), user.getUserProfileId());
        String fileName = String.format("%s-%s",file.getOriginalFilename(), UUID.randomUUID());
        try {
            fileStore.save(path, fileName, Optional.of(metadata), file.getInputStream());
            user.setUserProfileImageLink(fileName);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    public byte[] downloadUserProfileİmage(UUID userProfileId) {
        UserProfile user = getUserProfileorThrow(userProfileId);

        String path = String.format("%s/%s",
                BucketName.PROFILE_IMAGE.getBucketName(),
                user.getUserProfileId());

          return user.getUserProfileImageLink().
                  map(key -> fileStore.download(key, path))
                  .orElse(new byte[0]);

    }


    private UserProfile getUserProfileorThrow(UUID userProfileId) {
        UserProfile user = userProfileDataAccessService.getUserProfiles()
                .stream()
                .filter(userProfile -> userProfile.getUserProfileId().equals(userProfileId))
                .findFirst()
                .orElseThrow(()-> new IllegalStateException(String.format("User profile %s not found ",userProfileId)));
        return user;
    }

    private Map<String, String> extractMetadata(MultipartFile file) {
        Map<String,String> metadata = new HashMap<>();
        metadata.put("Content-Type",file.getContentType());
        metadata.put("Content-Length",String.valueOf(file.getSize()));
        return metadata;
    }

    private void isFileEmpty(MultipartFile file) {
        if(file.isEmpty()){
            throw new IllegalStateException("Cannt upload empty file [ " + file.getSize() + " ]");
        }
    }

    private void isİmage(MultipartFile file) {
        if(!Arrays.asList(
                IMAGE_JPEG.getMimeType(),
                IMAGE_PNG.getMimeType(),
                IMAGE_GIF.getMimeType()
        ).contains(file.getContentType())){
            throw new IllegalStateException("File must be an image");
        }
    }


}
