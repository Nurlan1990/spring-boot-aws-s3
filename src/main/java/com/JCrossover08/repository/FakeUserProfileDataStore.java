package com.JCrossover08.repository;

import com.JCrossover08.profile.UserProfile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class FakeUserProfileDataStore {

    private static final List<UserProfile> userProfile = new ArrayList<>();

    static {
        userProfile.add(new UserProfile(UUID.randomUUID(),"jonjones",null));
        userProfile.add(new UserProfile(UUID.randomUUID(),"danielcormiere",null));
    }

    public List<UserProfile> getUserProfile(){
        return userProfile;
    }

}
